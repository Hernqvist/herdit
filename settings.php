<?php 
	require_once("includes/session.php");
	require_once("includes/functions.php");
	$session = checkSess();

	

	if ($_SERVER['REQUEST_METHOD'] == "POST") {
		if (isset($_POST['acc-submit'])) {
			$username = $session[0]['username'];
			$name = $_POST['name-inp'];
			$gender = $_POST['gender-inp'];
			$age = $_POST['age-inp'];
			$location = $_POST['loc-inp'];
			changeProfile($username, $name, $gender, $age, $location);
		}

		if (isset($_POST['pass-submit'])) {
			$oldpass = $_POST['old-pass'];
			$newpass = $_POST['new-pass'];
			$rnewpass = $_POST['rnew-pass'];
			updateUser($session[0]['username'], $oldpass, $newpass, $rnewpass);
		}
	}
?>

<!DOCTYPE html>
<html>

	<?php 
		require("includes/header.php"); 
	?>

	<body>
		<aside>
		<?php require("includes/main.php"); ?>
		<div class="field-wrap">
			<h1><a href="index.php"><?= $session[0]['username'] ?> Account Settings</a></h1>
			<form method="POST">
				<h2>Change account information</h2>
				<input type="text" id="fullname" name="name-inp" placeholder="Full name" class="m-b">
				<select name="gender-inp" id="gender" class="m-b">
					<option value="" selected="trues">Gender...</option>
					<option value="Male">Male</option>
					<option value="Female">Female</option>
				</select>
				<input type="text" name="age-inp" id="age" placeholder="Birthyear" class="m-b">
				<input type="text" name="loc-inp" id="location" placeholder="Country" class="m-b">
				<input type="submit" value="Save changes" name="acc-submit">
			</form>
			<form method="POST" id="ch-pass-form">
				<h2>Change password</h2>
				<input type="password" id="o-pass" name="old-pass" placeholder="Old password" class="m-b">
				<input type="password" id="n-pass" name="new-pass" placeholder="New password" class="m-b">
				<input type="password" id="rn-pass" name="rnew-pass" placeholder="Repeat new password" class="m-b">
				<input type="button" id="ch-pass" value="Save changes" name="pass-submit">
			</form>
			<h2>Change avatar image</h2>
			<div class="profilepic-wrap"><img src=<?= showAvatar(false, ""); ?> alt="" class="profilepic"></div>
			<?php require("includes/uploadform.php"); ?>
			<h3 class="m-t"><a href="index.php">Back</a></h3>
		</div>
		</aside>
		<main>
			<?php 
				require("includes/postform.php");
			?>
		</main>
	</body>
</html>