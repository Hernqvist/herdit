<?php 

	function logout() {
		session_start();
		session_destroy();
		setcookie("authid", "", 0);
		setcookie("authkey", "", 0);

		session_start();

		header("Location: index.php");
	}
	logout();