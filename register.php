<?php 
	require_once("includes/functions.php");
	require_once("includes/session.php");
	$session = checkSess();
 ?>

<!DOCTYPE html>
<html>

	<?php 
		require("includes/header.php"); 
	?>

	<body>
		<aside>
		<?php require("includes/main.php"); ?>
			<div class="login">
				<form id="reg-form" action="register.php" method="POST">
					<h2>Username</h2> 
					<input type="text" id="name" name="userreg"><br>
					<h2>Password</h2> 
					<input type="password" id="pass" name="passreg">
					<h2>Confirm Password</h2> 
					<input type="password" id="cpass" name="conpassword"><br>
					<input type="button" id="register" value="Register">
				</form>
			<h3 class="m-t"><a href="index.php">Back</a></h3>
			<div id="errors"></div>
			</div>
				<?php createUser() ?>
		</aside>
		<main>
			<?php 
				require("includes/postform.php");
			?>
		</main>
	</body>
</html>

