<?php 

	require_once("includes/session.php");
	require_once("includes/functions.php");

	$session = checkSess();

	if ($session) {
		header("Location: index.php");
		die;
	}

	if ($_SERVER['REQUEST_METHOD'] == "POST") {
		$username	= $_POST['username'];
		$password	= $_POST['password'];
		$user 		= valUser($username, $password);

		if ($user) {
			createSess($user['username']);
			header("Location: index.php");
		} else {
			header("Location: index.php");
			errorRep("red", "Ogiltig login.");
		}
	}

	require("includes/loginform.php");