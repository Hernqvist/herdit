<?php 
header('Content-type: text/html; charset=utf-8');
	require_once("includes/session.php");
	require_once("includes/functions.php");

	$session = checkSess();
?>
<html>

	<?php 
		require("includes/header.php"); 
	?>

	<body>
	
	<aside>
	<?php 
	require("includes/main.php");
	if ($session) {
		require("includes/profile.php"); 
		require("includes/postform.html");
	} else {
		require("includes/loginform.php");
	}
	?>
	</aside>
	<main>
	<?php 
		require("includes/postform.php");
	?>
	</main>
	<?php	

	errorPrint();

	require("includes/footer.php");
	?>
	</body>
</html>