<?php 
	require_once("includes/functions.php");
	$session = checkSess();

	$limit = 10;
	$page = 0;
	$max = 0;
	$page = filter_input(INPUT_GET, "up");
	$max = dbGet("SELECT count(*) as max FROM users")[0]['max'];
	$max -= 1;
	$max = floor($max/$limit);

		if ($page) {
			if ($page > $max) {
				$page = $max;
			} else if ($page < 0) {
				$page = 0;
			}
	}
	$users = showUsers($limit, $page);
?>
<html>
	<?php 
		require_once("includes/header.php")
	?>
	<body>
		<aside>
 			<?php
 			require("includes/main.php");
			//print out registered users
			if (isset($_GET['user'])) {
				$user = $_GET['user'];
				$info = dbGet("SELECT * FROM profiles WHERE username = '{$user}'")[0];
			}
			foreach ($users as $u) {
				$pic = showAvatar(true, $u);
				$ru = rawurlencode($u);
				print "<div class='profileLink'>";
				print "<img src='$pic' alt='' class='profile-mini-profilepic' />";
				print "<p class='topic profile-user m-tb'><a href='?user={$ru}'>{$u}</a></p>";
				//if a username is clicked, if set: print profile information (bio)
				if (isset($user) && $user == $u) {
					print "<div class='bio m-b'>";
					print "<div class='name'>Name: {$info['name']}</div>";
					print "<div class='age'>Born: {$info['age']}</div>";
					print "<div class='gender'>Sex: {$info['gender']}</div>";
					print "<div class='country'>Country: {$info['country']}</div>";
					print "</div>";
				}
				print "</div>";
			} 
			?>
			<?php 
			$pages = ceil($max/$limit);
			print "<p class='pg-wrap'>Pages: ";
			for ($i = 1, $j = 0; $i <= $pages; $i++, $j++) {
				print "<a class='pg-num' href='?up=$j'>$i</a>";
			}
			print "</p>";
			?>
			<h3 class="m-t"><a href="index.php">Back</a></h3>
		</aside>
		<main>
			<?php 
				require("includes/postform.php");
			?>
		</main>
	</body>
</html>