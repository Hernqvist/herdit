-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2015 at 03:13 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `herdit`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `rating` int(11) NOT NULL DEFAULT '0',
  `postDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `topic`, `content`, `url`, `rating`, `postDate`) VALUES
(1, 1, 'First Post', 'Hit the link, it links to my beautiful website. :)', 'http://www.hernqvist.net', 3, '2015-02-26 11:28:19'),
(2, 3, 'Second Post', 'Hihi, beautiful, right?', 'http://www.hernqvist.net', 0, '2015-02-26 13:24:45'),
(3, 4, 'fhm', 'yay!', 'http://google.se', 0, '2015-02-26 15:05:37'),
(4, 5, 'Third Post', 'Woo.', 'http://www.hernqvist.net', 0, '2015-02-26 16:50:51'),
(5, 10, 'First Post', 'pkjhgfd', 'http://www.google.com', 0, '2015-03-03 14:01:38');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
`id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'Private',
  `gender` varchar(255) NOT NULL DEFAULT 'Private',
  `age` varchar(255) NOT NULL DEFAULT 'Private',
  `country` varchar(255) NOT NULL DEFAULT 'Private'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `username`, `name`, `gender`, `age`, `country`) VALUES
(1, 'prov2', 'Private', 'Private', 'Private', 'Private'),
(2, 'prov3', 'Private', 'Private', 'Private', 'Private'),
(3, 'prov1', 'Private', 'Private', 'Private', 'Private'),
(4, 'patrik', 'Private', 'Private', 'Private', 'Private'),
(5, 'emil', 'Private', 'Private', 'Private', 'Private'),
(6, 'emilemil', 'Private', 'Private', 'Private', 'Private'),
(7, 'prov6', 'Private', 'Private', 'Private', 'Private'),
(8, 'emileoi', 'Private', 'Private', 'Private', 'Private'),
(9, 'fedafaed', 'Private', 'Private', 'Private', 'Private'),
(10, '123123', 'Private', 'Private', 'Private', 'Wherever the road brings me...');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `hashKey` varchar(255) NOT NULL,
  `sessionkey` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `hashKey`, `sessionkey`) VALUES
(1, 'prov1', '58c192c71b2c6768b236bb7e8b91b734', '$2y$10$MDIwNGM1ZDQ5NzMzNDcyNj', '$1$Vm2.4v3.$Z3kLFe01CktewPVueov2P1'),
(2, 'prov2', '6ec351fcd19382684c7c042e790674cc', '$2y$10$NDg4YjQ5YWIxNDg0N2E5OD', '$1$DN3.Q5..$Uwc8gx0bb1uyOlalrdOzS1'),
(3, 'prov3', '0d808c6d83ca19f11f006cf6d582e03a', '$2y$10$N2RiNTI1NDIzOGNiZDI1NW', '$1$3c/.Od2.$.W57Zqpt9fbTjnA44ZoTH.'),
(4, 'patrik', 'faa2523bdc35f49204f8bf38d57ffe66', '$2y$10$MDZkMzRkODliOTUwMmJiMz', '$1$EF0.7N..$fJaJzHa/AkzFv1qLIrtSL1'),
(5, 'emil', '43e3069d349471cb84ee9fd8e484220d', '$2y$10$YWM4N2IwNmRjZGI0MDRhOW', '$1$4k1.59/.$8WWxRY8l2dBzjfOOMUhYf0'),
(6, 'emilemil', '72c90056596fc95d4995ff55a1993349', '$2y$10$NWQxOTdjYzNlY2Y3M2YzMz', ''),
(7, 'prov6', '59ba159cb22f85e0c21579fd2bc732ab', '$2y$10$MGQ3Yzk0N2UxNTkxNzcxNz', ''),
(8, 'emileoi', '928c48c9cbf644c5dd0428af5f36833b', '$2y$10$Y2U1ODhkOTQ5ZTEwZjc5Mm', ''),
(9, 'fedafaed', 'b9856bf42c321451e128b14299450a72', '$2y$10$MWEyNmFjM2U3MDYzZDQ2NW', ''),
(10, '123123', 'eace02f1769cdf710ca0e93cee17d6f0', '$2y$10$ZjIxYzA1YzE3NGZmYjk3Mj', '$1$c35./a0.$9OrciDtgH7YWEIVk1dkJJ/');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
