<?php 

	require_once("connection.php");
	require_once("session.php");

	function genSalt() {
		$uRandStr 	= md5(uniqid(mt_rand(), true)); 
		$base64Str 	= base64_encode($uRandStr);
		$mod64Str 	= str_replace("+", ".", $base64Str);
		$salt 		= substr($mod64Str, 0, 22);

		return $salt;
	}

	function passEnc($password) {
		$hashFormat = "$2y$10$";
		$salt 		= genSalt();
		$key 		= $hashFormat . $salt; 
		$hash 		= crypt($password, $key);
		$hash 		= md5($hash);

		return array("key" => $key, "hash" => $hash);
	}

	function valPass($input, $password, $key) {
		if (md5(crypt($input, $key)) == $password) {
			return true;
		}
		return false;
	}

	function valUser($username, $password) {
		$user = dbGet("SELECT * FROM users WHERE username = '$username'");
		if ($user) {
			$valPass = valPass($password, $user[0]['password'], $user[0]['hashKey']);
			if ($valPass) {
				$array = [
					"username" => $user[0]['username']
				];
				return $array;
			}
		}
		return false;
	}

	function createUser() {
		if (isset($_POST['userreg']) && isset($_POST['passreg'])) {
			$valid = true;
			$enc 		= passEnc($_POST['passreg']);
			$username 	= $_POST['userreg'];
			$con_pass 	= $_POST['conpassword'];
			$password	= $enc['hash'];
			$hashKey  	= $enc['key'];
			$array		= [
				"username" => $username, 
				"password" => $password, 
				"hashKey" => $hashKey
				];
			$dupe = dbGet("SELECT * FROM users WHERE username = '$username'");

			if (preg_match("/^.*(?=.{6,}).*$/", $_POST["passreg"]) === 0) {
				$valid = false;
			}
			if ($_POST['passreg'] != $con_pass) {
				$valid = false;
			}
			if ($valid == false) {
				return false;
			}

			if (!$dupe) {
				dbSet(
					"INSERT INTO users (username, password, hashKey) 
					 VALUES ('$username', '$password', '$hashKey')"
					 );
				dbSet(
					"INSERT INTO profiles (username) 
					 VALUES ('{$username}')"
					);
				copy("resources/img/minithumb/default.png", "resources/img/minithumb/$username.png");
				header("Location: index.php");
			} else {
				return false;
			}
		}
	}

	function updateUser($username, $old, $new, $rnew) {
		$user = dbGet("SELECT * FROM users WHERE username = '{$username}'");
		$valPass = valPass($old, $user[0]['password'], $user[0]['hashKey']);
		$isValid = true;
		if ($old == null || $old == "") {
			$isValid = false;
		}
		if ($new == null || $new == "") {
			$isValid = false;
		}
		if ($rnew == null || $rnew == "") {
			$isValid = false;
		}
		if ($valPass) {
			if ($new == $rnew) {
				$new = passEnc($new);
				$password = $new['hash'];
				$hashKey = $new['key'];
				dbSet("UPDATE users SET password = '{$password}', hashKey = '{$hashKey}' WHERE username = '{$username}'");
			} else {
				$isValid = false;
			}
		} else {
			$isValid = false;
		}

		if (!$isValid) {
			return false;
		}
	}

	function time2str($ts) {
	    if(!ctype_digit($ts))
	        $ts = strtotime($ts);

	    $diff = time() - $ts;
	    if($diff == 0)
	        return 'now';
	    elseif($diff > 0)
	    {
	        $day_diff = floor($diff / 86400);
	        if($day_diff == 0)
	        {
	            if($diff < 60) return 'just now';
	            if($diff < 120) return '1 minute ago';
	            if($diff < 3600) return floor($diff / 60) . ' minutes ago';
	            if($diff < 7200) return '1 hour ago';
	            if($diff < 86400) return floor($diff / 3600) . ' hours ago';
	        }
	        if($day_diff == 1) return 'Yesterday';
	        if($day_diff < 7) return $day_diff . ' days ago';
	        if($day_diff < 31) return ceil($day_diff / 7) . ' weeks ago';
	        if($day_diff < 60) return 'last month';
	        return date('F Y', $ts);
	    }
	    else
	    {
	        $diff = abs($diff);
	        $day_diff = floor($diff / 86400);
	        if($day_diff == 0)
	        {
	            if($diff < 120) return 'in a minute';
	            if($diff < 3600) return 'in ' . floor($diff / 60) . ' minutes';
	            if($diff < 7200) return 'in an hour';
	            if($diff < 86400) return 'in ' . floor($diff / 3600) . ' hours';
	        }
	        if($day_diff == 1) return 'Tomorrow';
	        if($day_diff < 4) return date('l', $ts);
	        if($day_diff < 7 + (7 - date('w'))) return 'next week';
	        if(ceil($day_diff / 7) < 4) return 'in ' . ceil($day_diff / 7) . ' weeks';
	        if(date('n', $ts) == date('n') + 1) return 'next month';
	        return date('F Y', $ts);
	    }
	}

	function createPost($user) {
		$params 	= [$_POST['topic'], $_POST['content'], $_POST['url']];
		list($t, $c, $u) = $params;
		$useridArray= dbGet("SELECT id FROM users WHERE username = '$user'");
		$userid		= $useridArray[0]['id'];
		
		if (mempty($t, $c, $u)) {
			if (!validateURL($u)) {
				return false;
			} else {
				dbSet("INSERT INTO posts (id, user_id, topic, content, url) 
							VALUES (id, '$userid', '$t', '$c', '$u')");
				return true;
			}
		} else {
			return false;
		}
	}

	function printPosts($limit, $page) {
		$search = "";
		if (isset($_GET['query'])) {
			$search = "AND topic LIKE '%{$_GET['query']}%'";
		}
		$query = ("SELECT posts.id, username, topic, content, url, rating, postDate 
						FROM users 
						INNER JOIN posts 
						WHERE users.id = posts.user_id $search ORDER BY rating DESC LIMIT $limit OFFSET ". ($page*$limit));
		$array = dbGet($query);
		return $array;
	}

	function validateVote() {
		$session = checkSess();
		if ($session) {
			if (isset($_POST['postid']) && isset($_POST['action'])) {

				$postId = $_POST['postid'];
				$authid = $session[0]['username'];
				# check if already voted, if voted then return
				if (isset($_SESSION["vote-". "$authid-". $postId]) || filter_input(INPUT_COOKIE, "vote-". "$authid-". $postId)) return;

				# query into db table to know current voting score 
				$data = dbGet(
							"SELECT rating
							FROM posts
							WHERE id = '{$postId}' 
							LIMIT 1"
							);

				# increase or decrease voting score
				if ($data) {
					$data = $data[0];
					if ($_POST['action'] === 'up'){
						$rating = ++$data['rating'];
					} else {
						$rating = --$data['rating'];
					}
					# update new voting score
					dbSet(
						"UPDATE posts
						SET rating = '{$rating}'
						WHERE id = '{$postId}'"
						);

					# set session with post id as true
					$_SESSION["vote-". "$authid-". $postId] = true; 
					setcookie("vote-". "$authid-". $postId, true, time()+60*60*24*30);
					# close db connection
					// TO DO
				}
			}
		}
	}

	function mempty() {
	    foreach(func_get_args() as $arg) {
	        if(!empty($arg)){
	            continue;
	        } else {
	            return false;
	        }
		}
	    return true;
	}

	function validateURL(&$u) {
		$u = trim($u, '!"#$%&\'()*+,-./@:;<=>[\\]^_`{|}~');
		if (strpos($u, "://") === false) {
			$u = "http://". $u;
		}
		if (!preg_match('%^(?:(?:http?|ftp)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6}))(?::\d+)?(?:[^\s]*)?$%iu', $u)) {
			return false;
		}
		return true;
	}


	function showAvatar($isMini, $user) {
		$session = checkSess();
		$u = $session[0]['username'];
		if ($isMini) {
			$file_path = "resources/img/minithumb/$user.png";
		} else {
			$file_path = "resources/img/thumbnails/$u.png";
		}
		if (file_exists($file_path)) {
			return $file_path;
		} else {
			return "resources/img/defaultavatar.png";
		}
	}

	function getUsers($limit, $page) {
		$users = dbGet("SELECT username FROM users LIMIT $limit OFFSET ". ($page*$limit));
		return $users;
	}

	function showUsers($limit, $page) {
		$session = checkSess();
		$uarray = getUsers($limit, $page);
		foreach ($uarray as $users) {
			$users = $users['username'];
			if ($session[0]['username'] == $users) {
				$result[] = $users;
			} else {
				$result[] = $users;
			}
		}
		return $result;
	}

	function errorRep($color, $string) {
		$_SESSION['messages'] = array(
			array(
				"status" => $color, "text" => $string
			),
		);
	}

	function errorPrint() {
		if (isset($_SESSION['messages'])) {
			foreach ($_SESSION['messages'] as $msg) {
				print "<div class='errors'><p style='color:" . $msg['status'] . "'>" . $msg['text'] . "</p></div>";
			}
		}	
		unset($_SESSION['messages']);
	}

	function changeProfile($username, $name, $gender, $age, $location) {

		$inValid = false;

		$list = [];
		if ($name) {
			$list[] = "name = '{$name}'";
			$inValid = true;
		}
		if ($gender) {
			$list[] = "gender = '{$gender}'";
			$inValid = true;
		}
		if ($age) {
			$list[] = "age = '{$age}'";
			$inValid = true;
		}
		if ($location) {
			$list[] = "country = '{$location}'";
			$inValid = true;
		}

		$set = "";
		for($i = 0 ; $i < count($list) ; $i++) {
			if ($i != 0) {
				$set .= ", ";
			}
			$set .= $list[$i];
		}

		if (!$inValid) {
			return false;
		} else {
			$dupe = dbGet("SELECT * FROM profiles WHERE username = '{$username}'");
			if ($dupe) {
				dbSet( 
					"UPDATE profiles 
					 SET $set
					 WHERE username = '{$username}'"
					 );
			} 
		}
	}
?>