<div class="login">
	<form id="login-form" action="login.php" method="POST">
		<div class="input-wrap">
		    <h2>Username</h2> 
		    	<input type="text" id="name" name="username" /><br/>
		    <h2>Password</h2> 
		    	<input type="password" id="pass" name="password" />
		    <input type="button" id="login" value="Login">
		</div>
		<div class="reg-link-wrap"><p class="reg-text">Need an account? <a class="reg-link" href="register.php">Click here</a>.</p></div>
	</form>
</div>