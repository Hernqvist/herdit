<?php 
	function validateAvatar() {
		if (!empty($_POST['upload-submit'])) {	
			if (isset($_FILES['upload'])) {
				$file = $_FILES['upload']['tmp_name'];
				$name = $_FILES['upload']['name'];
				$error = $_FILES['upload']['error'];
			if ($_FILES['upload']['size'] > 20971520) {
				$error = 1;
			}
				if ($error == 0) {
					return $file;
				} else {
					return false;
				}
			}
		}
	}

	// define('$max_width', 150);
	// define('$max_height', 150);

	function thumbgen($imgpath, $newimgpath, $max_width, $max_height)
	{
	    list($imgwidth, $imgheight, $imgtype) = getimagesize($imgpath);
	    switch ($imgtype) {
	        case IMAGETYPE_GIF:
	            $gdimg = imagecreatefromgif($imgpath);
	            break;
	        case IMAGETYPE_JPEG:
	            $gdimg = imagecreatefromjpeg($imgpath);
	            break;
	        case IMAGETYPE_PNG:
	            $gdimg = imagecreatefrompng($imgpath);
	            break;
	    }
	    if ($gdimg === false) {
	        return false;
	    }
	    $asprate = $imgwidth / $imgheight;
	    $thumbasprate = $max_width / $max_height;
	    if ($imgwidth <= $max_width && $imgheight <= $max_height) {
	        $thumbimgwidth = $imgwidth;
	        $thumbimgheight = $imgheight;
	    } elseif ($thumbasprate > $asprate) {
	        $thumbimgwidth = (int) ($max_height * $asprate);
	        $thumbimgheight = $max_height;
	    } else {
	        $thumbimgwidth = $max_width;
	        $thumbimgheight = (int) ($max_width / $asprate);
	    }
	    $thumbnail_gd_image = imagecreatetruecolor($thumbimgwidth, $thumbimgheight);
	    imagecopyresampled($thumbnail_gd_image, $gdimg, 0, 0, 0, 0, $thumbimgwidth, $thumbimgheight, $imgwidth, $imgheight);
	    imagejpeg($thumbnail_gd_image, $newimgpath, 90);
	    imagedestroy($gdimg);
	    imagedestroy($thumbnail_gd_image);
	    return true;
	}

	function uploadAvatar() {
		$session = checkSess();
		$user = $session[0]['username'];
		if (!validateAvatar()) {
			return false;
		} else {
			$file = validateAvatar();
			move_uploaded_file($file, "resources/img/profilepics/$user.png");
			thumbgen("resources/img/profilepics/$user.png", "resources/img/thumbnails/$user.png", 150, 150);
			thumbgen("resources/img/profilepics/$user.png", "resources/img/minithumb/$user.png", 20, 20);
			unlink("resources/img/profilepics/$user.png");
			header("Location: settings.php");	
		}
	}
	uploadAvatar();
 ?>
<form method="POST" enctype="multipart/form-data" name="uploadForm">
    <h3>Select image to upload:</h3>
    <input type="file" name="upload" id="fileToUpload">
    <input type="submit" value="Upload Image" name="upload-submit">
</form>