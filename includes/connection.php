<?php 

	define("SERVER", "localhost");
	define("USER", "root");
	define("PASS", "");

	function dbConnect($db = "herdit") {
		
		$con = new PDO("mysql:host=" . SERVER . ";dbname=" . $db. ";charset=utf8;", USER, PASS);
		$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		return $con;
	}

	function dbGet($query) {
		try {
			$con	= dbConnect();
			$stmt	= $con->prepare($query);
			$stmt->execute();
			$array	= [];
			
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$array[] = $row;
			}
			if (!empty($array)) {
				return $array;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			print "ERROR:" . $e->getMessage();
		}
	}

	function dbSet($query) {

		try {
			$con = dbConnect();
			$stmt = $con->prepare($query);
			$stmt->execute();
			if ($stmt->rowCount() >= 1) {
				return true;
			}
			return false;
		} catch (PDOException $e) {
			print "ERROR:" . $e->getMessage();
		}
	}