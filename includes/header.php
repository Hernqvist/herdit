	<head>
		<title>Herdit</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
		<meta name="description" content="">

		<link rel="stylesheet" href="resources/css/base.css" type="text/css">
		<link rel="stylesheet" href="resources/css/style.css" type="text/css">
		<link rel="shortcut icon" href="/favicon.ico" type="image/ico">
		<script src="resources/js/jquery-1.11.1.min.js"></script>
		<script src="includes/script.js"></script>
		<script src="resources/js/ajaxSearch.js"></script>
		<script src="resources/js/herdit.js"></script>

	</head>