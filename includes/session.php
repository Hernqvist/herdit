<?php 

	require_once("connection.php");
	session_start();

	function checkSess() {
		$sessionid 	= "";
		$sessionkey = "";

		if (isset($_SESSION['authid'])) {
			$sessionid = $_SESSION['authid'];
		} else {
			$sessionid = filter_input(INPUT_COOKIE, "authid", FILTER_SANITIZE_SPECIAL_CHARS);
		}
		if (isset($_SESSION['sessionkey'])) {
			$sessionkey = $_SESSION['sessionkey'];
		} else {
			$sessionkey = filter_input(INPUT_COOKIE, "sessionkey");
		}
		if ($sessionid == "" || $sessionkey == "") {
			return false;
		}

		$user = dbGet("SELECT * FROM users WHERE sessionkey = '$sessionkey'");

		if (!$user) {
			return false;
		}
		if ($sessionkey == $user[0]['sessionkey']) {
			return $user;
		} else {
			return false;
		}
	}

	function createSess($username) {
		$length = 42;
		$rawkey = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
		$sessionkey = crypt($rawkey);
		if (dbSet("UPDATE users SET sessionkey = '$sessionkey' WHERE username = '$username'")) {
			$_SESSION['authid'] = $username;
			$_SESSION['sessionkey'] = $sessionkey;
			setcookie("authid", $username, time()+(60*60*24*30));
			setcookie("sessionkey", $sessionkey, time()+(60*60*24*30));
		}
	}


