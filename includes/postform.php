<?php 
	header('Content-type: text/html; charset=utf-8');
	require_once("session.php");
	require_once("functions.php");

	
	$session = checkSess();
	
	if (!empty($_POST['topic'])) {
		createPost($session[0]['username']);
	}

	$limit = 6;
	$page = 0;
	$page = filter_input(INPUT_GET, "pp");
	$max = dbGet("SELECT count(*) as number FROM posts")[0]['number'];
		if ($page) {
			if ($page > ceil($max/$limit)) {
				$page = ceil($max/$limit);
			} else if ($page < 0) {
				$page = 0;
			}
	}

	$postArray = printPosts($limit, $page);
	?>

		<div id="topics" class='topics'>
		<?php 
		foreach($postArray as $posts): ?>

			<div class='post' post-id='<?= $posts['id']; ?>'>
					<div class='rating' data-postid='<?= $posts['id']; ?>' data-score='<?= $posts['rating'] ?>'>
						<div class='vote-span'>
							<div <?php if($session){print "class='vote'";}; ?> data-action='up' title='Vote up'>
								<i class=''>▲</i>
							</div>
							<div class='vote-score'><?= $posts['rating']; ?></div>
							<div <?php if($session){print "class='vote'";}; ?> data-action='down' title='Vote down'>
								<i class=''>▼</i>
							</div><!--vote down-->
						</div>
					</div>
					<div class="content"><p class="topic"><a href="<?= $posts['url'] ?>" class=''><?= $posts['topic']; ?></a></p><p class="post-content"><?= $posts['content']; ?></p></div>
					

					<div class="date-wrap">
						<p class="post-date">Posted <?= time2str($posts['postDate'])?> by 
							<?= "<a href='profile.php?user=". $posts['username']. "'>". $posts['username']. "</a>" ?>
						</p>
						<a href="profile.php?user=<?= $posts['username'] ?>">
							<span class="helper"></span><img src=<?= "resources/img/minithumb/{$posts['username']}.png" ?> alt="" class="mini-profilepic">
						</a>
					</div>
			</div>
		<?php endforeach ?>
		<?php  

		$pages = ceil($max/$limit);
		print "<p class='pg-wrap'>Pages: ";
		for ($i = 1, $j = 0; $i <= $pages; $i++, $j++) {
			print "<a class='pg-num' href='?pp=$j'>$i</a>";
		}
		print "</p>"
		?>
		</div>