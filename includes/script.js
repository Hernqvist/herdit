
$(document).ready(function(){
	// ajax setup
	$.ajaxSetup({
		url: "includes/vote.php",
		type: "POST",
		cache: "false"
	});

	// any voting button (up/down) clicked event
	$('.vote').click(function(){
		var self = $(this); // cache $this
		var action = self.data('action'); // grab action data up/down 
		var parent = self.parent().parent(); // grab grand parent .item
		var postid = parent.data('postid'); // grab post id from data-postid
		var score = parent.data('score'); // grab score from data-score

		// only works where is no disabled class
		if (!parent.hasClass('.disabled')) {
			// vote up action
			if (action == 'up') {
				// increase vote score and color to #2EE021
				parent.find('.vote-score').html(++score).css({'color':'#2EE021'});
				// change vote up button color to #2EE021
				self.css({'color':'#2EE021'});
				// send ajax request with post id & action
				$.ajax({data: {'postid' : postid, 'action' : 'up'}});
			}
			// voting down action
			else if (action == 'down'){
				// decrease vote score and color to red
				parent.find('.vote-score').html(--score).css({'color':'red'});
				// change vote up button color to red
				self.css({'color':'red'});
				// send ajax request
				$.ajax({data: {'postid' : postid, 'action' : 'down'}});
			};

			// add disabled class with .item
			parent.addClass('.disabled');
		};
	});
});

