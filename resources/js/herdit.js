$(document).ready(function(){

	$("input").keyup(function(){
		$(this)
			.css("border-color", "");
	});
	$("textarea").keyup(function(){
		$(this)
			.css("border-color", "");
	});

	$("#register").click(function(){
		var isValid = true;
		var name = $("#name").val();
		var password = $("#pass").val();
		var cpassword = $("#cpass").val();

		if (name == "") {
			$("#name").css("border-color", "#e74c3c");
			isValid = false;
		}
		if (password == "" || password.length < 6) {
			$("#pass").css("border-color", "#e74c3c");
			isValid = false;
		}
		if (cpassword == "" || password.length < 6) {
			$("#cpass").css("border-color", "#e74c3c");
			isValid = false;
		}
		if (password != cpassword) {
			$("#pass").css("border-color", "#f1c40f");
			$("#cpass").css("border-color", "#f1c40f");
			isValid = false;
		}

		if (isValid == true) {
			$("#reg-form").submit();
		}
	});

	$("#login").click(function(){
		var isValid = true;
		var name = $("#name").val();
		var password = $("#pass").val();

		if (name == "") {
			$("#name").css("border-color", "#e74c3c");
			isValid = false;
		}
		if (password == "" || password.length < 6) {
			$("#pass").css("border-color", "#e74c3c");
			isValid = false;
		}

		if (isValid == true) {
			$("#login-form").submit();
		}
	});

	$("#post").click(function() {
		var isValid = true;
		var topic = $("#input-topic").val();
		var url = $("#input-url").val();
		var content = $("#content").val();

		if (topic == "" || topic.length > 15) {
			$("#input-topic").css("border-color", "#e74c3c");
			isValid = false;
		}
		if (url == "" || url.length > 200) {
			$("#input-url").css("border-color", "#e74c3c");
			isValid = false;
		}
		if (content == "" || content.length > 255) {
			$("#content").css("border-color", "#e74c3c");
			isValid = false;
		}

		if (isValid == true) {
			$("#post-form").submit();
		}
	});

	$("#ch-pass").click(function(){
		var isValid = true;
		var opassword = $("#o-pass").val();
		var npassword = $("#n-pass").val();
		var rnpassword = $("#rn-pass").val();

		if (opassword == "") {
			$("#o-pass").css("border-color", "#e74c3c");
			isValid = false;
		}
		if (npassword == "" || npassword.length < 6) {
			$("#n-pass").css("border-color", "#e74c3c");
			isValid = false;
		}
		if (rnpassword == "" || npassword.length < 6) {
			$("#rn-pass").css("border-color", "#e74c3c");
			isValid = false;
		}
		if (npassword != rnpassword) {
			$("#npass").css("border-color", "#f1c40f");
			$("#rnpass").css("border-color", "#f1c40f");
			isValid = false;
		}

		if (isValid == true) {
			$("#ch-pass-form").submit();
		}
	});

});
